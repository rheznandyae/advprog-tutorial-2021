package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack(){
        return "pew pew pew";
    }

    @Override
    public String getType() {
        return "Attack with Gun";
    }
}
