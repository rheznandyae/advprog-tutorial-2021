package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    //ToDo: Complete me
    public String defend() {
        return "Armor defend";
    }

    public String getType() {
        return "Armor";
    }

}
