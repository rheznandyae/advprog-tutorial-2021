package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    protected ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }
    @Override
    public void cast() {
        // cast nothing
        for(Spell spell : this.spells){
            spell.cast();
        }

    }

    @Override
    public void undo() {
        // undo nothing
        for (int i = this.spells.size() - 1; i >= 0 ; i--) {
            this.spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
